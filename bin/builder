#!/usr/bin/env bash

## usage
## asdk_builder -p node-16 -T debian -a node -b "BASE_IMAGE=node:16" -b "JDK8_MIRROR=deb http://security.debian.org/debian-security stretch/updates main"
function asdk_builder() {
    set -e
    set -o pipefail

    local IMAGE_NAME ANDROID_API_VERSIONS ANDROID_BUILD_TOOLS_VERSION CMDLINE_TOOLS_VERSION EXEC_PATH REGISTRY_HOST config_dir=${XDG_CONFIG_HOME:-$HOME/.config}

    [[ -r /etc/asdk/config.conf ]] && source /etc/asdk/config.conf
    [[ -r $config_dir/asdk/config.conf ]] && source "$config_dir/asdk/config.conf"

    IMAGE_NAME=${ASDK_IMAGE_NAME:-$IMAGE_NAME}
    ANDROID_API_VERSIONS=${ASDK_ANDROID_API_VERSIONS:-$ANDROID_API_VERSIONS}
    ANDROID_BUILD_TOOLS_VERSION=${ASDK_ANDROID_BUILD_TOOLS_VERSION:-$ANDROID_BUILD_TOOLS_VERSION}
    CMDLINE_TOOLS_VERSION=${ASDK_CMDLINE_TOOLS_VERSION:-$CMDLINE_TOOLS_VERSION}
    EXEC_PATH=${ASDK_EXEC_PATH:-$EXEC_PATH}
    REGISTRY_HOST=${ASDK_REGISTRY_HOST:-$REGISTRY_HOST}

    local asdk_build
    if [[ -r "$EXEC_PATH/build" && -x "$EXEC_PATH/build" ]]; then
        asdk_build="$EXEC_PATH/build"
    else
        asdk_build=$(type -p asdk-build)
    fi

    local prefix aliases=() template build_args=() api_versions=() OPTIND
    while getopts ":p:T:b:a:" option; do
        case $option in
            p)
                prefix=$OPTARG >&2
                ;;
            T)
                template=$OPTARG >&2
                ;;
            b)
                build_args+=("-b" "$OPTARG") >&2
                ;;
            a)
                aliases+=("-a" "${OPTARG}") >&2
                echo "RECEIVED -a $OPTARG"
                ;;
            \?)
                echo "invalid option -$OPTARG"
                exit 1
                ;;
        esac
    done

    mapfile -td , api_versions <<< "$ANDROID_API_VERSIONS"
    api_versions=("${api_versions[@]%$'\n'}")

    ## append private registry host to image name
    [[ -n "$REGISTRY_HOST" ]] && IMAGE_NAME="${REGISTRY_HOST}/$IMAGE_NAME"

    $asdk_build -i "$IMAGE_NAME" -p "$prefix" -T "$template" -v "cmdline" "${aliases[@]}" -b "CMDLINE_TOOLS_VERSION=$CMDLINE_TOOLS_VERSION" "${build_args[@]}"
    $asdk_build -i "$IMAGE_NAME" -p "$prefix" -T "$template" -v "tools" "${aliases[@]}" -b "BASE_IMAGE=${IMAGE_NAME}:${prefix}-cmdline" -b "ANDROID_BUILD_TOOLS_VERSION=$ANDROID_BUILD_TOOLS_VERSION"
    for api_version in "${api_versions[@]}"; do
        $asdk_build -i "$IMAGE_NAME" -p "$prefix" -T "$template" -v "api-x" -s "api-${api_version,,}" "${aliases[@]}" -b "BASE_IMAGE=${IMAGE_NAME}:${prefix}-tools" -b "ANDROID_API_VERSION=$api_version"
    done
}

asdk_builder "$@"
